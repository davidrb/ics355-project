#ifndef ACPI_SUBMISSION_HPP
#define ACPI_SUBMISSION_HPP

#include <nlohmann/json.hpp>

#include <string>
#include <chrono>

namespace acpi {
  auto get_result(
    std::string const& source,
    std::chrono::milliseconds compile_timelimit,
    std::string const& test_case,
    std::chrono::milliseconds timelimit)
  -> nlohmann::json;
}

#endif
