#ifndef ACPI_EXCEPTIONS_HPP
#define ACPI_EXCEPTIONS_HPP

#include <stdexcept>
#include <string>

namespace acpi {
  class error : public std::runtime_error{
  public:
    error() : std::runtime_error("acpi error") {}

    virtual auto type() -> std::string = 0;
    virtual auto phase() -> std::string = 0;
    virtual auto msg() -> std::string = 0;

    virtual ~error() {}
  };

  class compilation_error : public error {
  public:
    compilation_error(std::string const& s) : msg_{s} {}

    auto type() -> std::string { return "error"; }
    auto phase() -> std::string { return "compilation"; }
    auto msg() -> std::string { return msg_; }

  private:
    std::string msg_;
  };

  class compilation_timeout : public error {
    auto type() -> std::string { return "timeout"; }
    auto phase() -> std::string { return "compilation"; }
    auto msg() -> std::string { return "compilation timed out"; }
  };
}

#endif
