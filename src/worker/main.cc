//TODO: synchronize cout access

#include "submission.hpp"

#include <pthread.h>

#include <boost/asio.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <vector>
#include <nlohmann/json.hpp>

using namespace std::string_literals;

namespace asio = boost::asio;

using json = nlohmann::json;

auto parse_config(const char* path) {
  auto config_file = std::ifstream{path};
  auto config = json{};
  config_file >> config;

  for (auto const& [k, v] : config["problems"].items())
    std::cerr << "problem '" << k << "': " << v << '\n';

  return config;
}

void on_command(std::stringstream ss, json const& config) try {
  auto problem = std::string{};
  auto source = std::string{};
  auto test_case = std::string{};

  ss >> problem >> source >> test_case;

  if (config["problems"].find(problem) == config["problems"].end())
    throw std::runtime_error{"no such problem "s + problem + " in config"};

  auto problem_json = config["problems"][problem];
  auto timelimit = std::chrono::milliseconds{problem_json["timelimit"]};

  std::cout << "> started " << source << " " << test_case << std::endl;

  auto result = acpi::get_result(
    source,
    std::chrono::seconds{5},
    test_case,
    timelimit
  );

  std::cout << result.dump(2) << std::endl;

} catch (std::runtime_error& err) {
  std::cout << "> caught exception: " << err.what() << '\n';
}

void pin_to_core(int i) {
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(i, &cpuset);

  pthread_setaffinity_np(
    pthread_self(),
    sizeof(cpu_set_t), &cpuset);
}

auto start_thread_pool(asio::io_context& io) -> std::vector<std::thread> {
  pin_to_core(0);

  auto thread_pool = std::vector<std::thread>{};

  for (auto i = 1u; i <= 3u; ++i) {
    thread_pool.emplace_back(std::thread{[&io, i](){
      pin_to_core(i);
      io.run();
    }});
  }

  return thread_pool;
}

int main(int argc, char *argv[]) {
  auto config = parse_config(argc > 1 ? argv[1] : "config.json");

  auto io = asio::io_service{};
  auto work = asio::io_service::work{io};

  auto thread_pool = start_thread_pool(io);

  std::cerr << "> " << std::flush;
  std::string command{};

  while (std::getline(std::cin, command)) {
    io.dispatch([command, config]() {
      on_command(std::stringstream{std::move(command)}, config);
    });

    std::cerr << "> " << std::flush;
  }
  std::cerr << "\n";

  io.stop();
  for (auto& t : thread_pool)
    t.join();
}
