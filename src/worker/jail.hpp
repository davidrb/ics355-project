#ifndef ACPI_JAIL_HPP
#define ACPI_JAIL_HPP

#include <string>
#include <map>
#include <chrono>
#include <mutex>
#include <thread>
#include <memory>

#include <boost/process/child.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>

namespace acpi {
  using output_stream = boost::iostreams::stream<boost::iostreams::file_descriptor_source>;

  class jail {
  public:
    static auto create() -> std::unique_ptr<acpi::jail>;

    void compile(
      std::string const& src,
      std::chrono::milliseconds timeout);

    auto run(
      std::string const& stdin_file,
      std::chrono::milliseconds timelimit)
      -> output_stream;

    void join();

    bool did_timeout();
    bool did_runtime_error();

    ~jail();

  private:
    bool runtime_error_;
    bool killed_;
    std::string bin_name_;
    std::thread kill_thread_;

    jail() {}
    jail(jail const&) = delete;
    auto operator=(jail const&) -> jail& = delete;
  };
}

#endif
