#include "jail.hpp"

#include "exceptions.hpp"

#include <boost/process/io.hpp>

#include <sched.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <iostream>
#include <string>
#include <random>

using namespace std::string_literals;
namespace iostreams = boost::iostreams;
namespace bp = boost::process;

using iostreams::file_descriptor_source;
using iostreams::file_descriptor_flags;
using iostreams::stream;

namespace acpi {
  struct child_fn_args {
    int out_pipe[2];
    int new_id;
    std::string path;
    std::string input_path;
  };
}

static int child_fn(void *arg) {
  auto args = static_cast<acpi::child_fn_args *>(arg);

  auto in_fd = open(args->input_path.c_str(), O_RDONLY);
  dup2(in_fd, 0);

  close(args->out_pipe[0]);
  dup2(args->out_pipe[1], 1);

  close(2);

  mount(nullptr, "/", nullptr, (MS_PRIVATE | MS_REC), nullptr);

  chdir("./jail_root");
  syscall(SYS_pivot_root, "." , "./old_root");
  chroot(".");
  chdir("/");

  mount("proc", "/proc", "proc", 0, nullptr);

  umount2("/old_root", MNT_DETACH);
//  rmdir("/old_root");

  // TODO: verify (in correct order)
  setuid(args->new_id);
  setgid(args->new_id);

  char* argv0 = (char *)args->path.c_str();
  char* argv[] = {argv0, nullptr};
  execvp(argv[0], argv);

  auto e = errno;
  throw std::runtime_error{ "failed to exec:"s + argv0 + ": " + strerror(e) };
}

static auto random_filename() -> std::string {
  auto length = 20;

  static auto& chrs = "0123456789"
    "abcdefghijklmnopqrstuvwxyz"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  thread_local static std::mt19937 rg{std::random_device{}()};
  thread_local static std::uniform_int_distribution<std::string::size_type> pick(0, sizeof(chrs) - 2);

  auto s = std::string{};
  s.reserve(length);

  while(length--) s += chrs[pick(rg)];

  return s;
}

void acpi::jail::compile(
  std::string const& source,
  std::chrono::milliseconds timeout) {

  bin_name_ = random_filename();

  auto cpu = std::to_string(sched_getcpu());
  auto err = bp::ipstream{};

  bp::child gcc{
    "/usr/bin/taskset", "-c", cpu,
                        "/usr/bin/g++", "--static", source,
                                        "-o", "jail_root/"s + bin_name_,
    bp::std_out > bp::null,
    bp::std_err > err
  };

  if (!gcc.wait_for(timeout)) {
    gcc.terminate();
    throw acpi::compilation_timeout{};
  }

  if (gcc.exit_code() != 0) {
    auto error_msg = std::string{std::istreambuf_iterator<char>{err}, {}};
    throw acpi::compilation_error{std::move(error_msg)};
  }
}

auto acpi::jail::run(
  std::string const& stdin_file,
  std::chrono::milliseconds timeout) -> acpi::output_stream {

  if (bin_name_.length() == 0)
    throw std::runtime_error{ "binary not compiled" };

  struct stat st = {0};
  if (stat("jail_root/old_root", &st) == -1)
    mkdir("jail_root/old_root", 0700);

  int out_pipe[2];
  pipe(out_pipe);

  auto args = acpi::child_fn_args{
    .out_pipe = {out_pipe[0], out_pipe[1]},
    .new_id = 1000,
    .path = "/"s + bin_name_,
    .input_path = stdin_file
  };

  auto constexpr child_stack_size = 1024;
  auto child_stack = new unsigned char[child_stack_size];

  auto child_pid = clone(
    child_fn,
    &child_stack[0] + child_stack_size,
    CLONE_NEWNET | CLONE_NEWNS | CLONE_NEWPID | SIGCHLD,
    &args);

  kill_thread_ = std::thread{[&, timeout](){
    auto child = bp::child{child_pid};

    killed_ = !child.wait_for(timeout);
    if (killed_) child.terminate();

    runtime_error_ = child.exit_code() != 0;
  }};

  close(out_pipe[1]);

  auto stdout_source = file_descriptor_source{
    out_pipe[0],
    file_descriptor_flags::close_handle
  };

  return stream<file_descriptor_source>{std::move(stdout_source)};
}

void acpi::jail::join() {
  if (kill_thread_.joinable())
    kill_thread_.join();
}

auto acpi::jail::did_timeout() -> bool { return killed_; }
auto acpi::jail::did_runtime_error() -> bool { return runtime_error_; }

auto acpi::jail::create() -> std::unique_ptr<acpi::jail> {
  return std::unique_ptr<acpi::jail>{new acpi::jail{}};
}

acpi::jail::~jail() {
  join();
  unlink(("jail_root/"s + bin_name_).c_str());
}

