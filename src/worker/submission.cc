#include "submission.hpp"
#include "jail.hpp"
#include "exceptions.hpp"

#include <nlohmann/json.hpp>

#include <string>
#include <fstream>
#include <algorithm>
#include <chrono>
#include <thread>

auto acpi::get_result(
  std::string const& source,
  std::chrono::milliseconds compile_timelimit,
  std::string const& test_case,
  std::chrono::milliseconds timelimit)
-> nlohmann::json try {

  auto ans_file = std::ifstream{test_case + ".ans"};

  auto jail = acpi::jail::create();

  jail->compile(source, compile_timelimit);
  auto out = jail->run(test_case + ".in", timelimit);

  auto correct = std::equal(
    std::istreambuf_iterator<char>{out}, {},
    std::istreambuf_iterator<char>{ans_file}, {});

  jail->join();

  if (jail->did_timeout())
    return { {"result", "timelimit"} };

  if (jail->did_runtime_error())
    return { {"result", "runtime_error"} };

  return {
    {"result", correct ? "correct" : "incorrect"}
  };

} catch(acpi::error& err) {
  return {
    {"result", "error"},
    {"error", {
      {"phase", err.phase()},
      {"type", err.type()},
      {"msg", err.msg()}
    }}
  };
}
